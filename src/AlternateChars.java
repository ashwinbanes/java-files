import java.io.*;

public class AlternateChars {

    public static void main(String[] args) throws IOException {

        File sourceFile = new File("test.txt");

        BufferedReader reader = new BufferedReader(new FileReader(sourceFile));

        AlternateCharReader reader1 = new AlternateCharReader(reader);

        String arr = reader1.readLine();

        File destFile = new File("dest.txt");

        try {
            destFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        InputStream is = null;
        OutputStream os = null;

        try {
            is = new FileInputStream(sourceFile);
            os = new FileOutputStream(destFile);
            byte[] buffer = new byte[1024];
            int length;
            for (int i = 0; i < arr.length(); i++) {
                os.write(arr.charAt(i));
            }
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
}
