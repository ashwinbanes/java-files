import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.util.Iterator;


public class Xml2Json {

    public static void main(String[] args) {

        File file = new File("example.xml");

        InputStream is = null;

        try {
            try {
                is = new FileInputStream(file);
                byte[] buffer = new byte[2048];
                int length;
                try {
                    length = is.read(buffer);
                    String str = new String(buffer);
                    JSONObject jsonObject = XML.toJSONObject(str);
                    JSONArray jsonArray = new JSONArray();
                    Iterator x = jsonObject.keys();
                    while (x.hasNext()) {
                        String key = (String) x.next();
                        System.out.println(key);
                        jsonArray.put(jsonObject.get(key));
                    }
                    String destFile = "xml2json.json";
                    writeToFile(destFile, jsonArray.toString(4));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void writeToFile(String destFile, String jsonString) {
        File file = new File(destFile);
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            writer.write(jsonString);
        }catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}