import java.io.BufferedReader;
import java.io.IOException;

public class AlternateCharReader extends BufferedReader {

    private BufferedReader reader;

    public AlternateCharReader(BufferedReader in) {
        super(in);
        this.reader = in;
    }

    @Override
    public String readLine() throws IOException {

        char[] arr = null;

        try {
            String str;

            while ((str = reader.readLine()) != null) {
                break;
            }

            arr = new char[str.length()];

            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                if (i % 2 == 0) {
                    arr[i] = Character.toLowerCase(ch);
                }else {
                    arr[i] = Character.toUpperCase(ch);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        String result = new String(arr);
        return result;
    }
}
