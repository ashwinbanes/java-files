import java.io.*;

public class FileCreation {

    private static String FILE_NAME_1 = "test1.txt";

    private static String FILE_NAME_2 = "test2.txt";

    private static String COPIED_FILE_NAME = "newtest.txt";

    public static void main(String[] args) {

        File file1, file2;
        FileWriter writer = null;

        file1 = new File(FILE_NAME_1);
        file2 = new File(FILE_NAME_2);

        try {

            if (file1.createNewFile()) {
                writer = new FileWriter(file1);
                writer.write("Test File 1");
                writer.close();
            }

            if (file2.createNewFile()) {
                writer = new FileWriter(file2);
                writer.write("Test file 2");
                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        File destFile = new File(COPIED_FILE_NAME);

        try {
            if (destFile.createNewFile()) {
                InputStream inputStream1 = null;
                InputStream inputStream2 = null;
                OutputStream outputStream = null;
                try {
                    inputStream1 = new
                            FileInputStream(FILE_NAME_1);
                    inputStream2 = new
                            FileInputStream(FILE_NAME_2);
                    outputStream = new
                            FileOutputStream(COPIED_FILE_NAME);
                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = inputStream1.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, length);
                    }

                    while ((length = inputStream2.read(buffer)) > 0) {
                        outputStream.write(buffer, 0, length);
                    }
                }finally {
                    inputStream1.close();
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            file1.delete();
        }
    }

}
